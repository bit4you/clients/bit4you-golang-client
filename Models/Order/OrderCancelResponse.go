package Order

type OrderCancelResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}