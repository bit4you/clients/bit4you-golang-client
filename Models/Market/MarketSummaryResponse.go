package Market

type MarketSummaryResponse []struct {
	Market    string `json:"market"`
	MarketCap string `json:"marketCap"`
	Supply    string `json:"supply"`
	Open      string `json:"open"`
	High      string `json:"high"`
	Low       string `json:"low"`
	Last      string `json:"last"`
	PrevDay   string `json:"prevDay"`
	Volume    string `json:"volume"`
	Bid       string `json:"bid"`
	Ask       string `json:"ask"`
}
