package Market
type MarketOrderBookRequest struct {
	Market      string      `json:"market"`
	Limit       int         `json:"limit"`
	State       bool        `json:"state"`
	ClientID    string `json:"clientId"`
	TimingForce string `json:"timingForce"`
}
