package Portfolio

type PortfolioCreateOrder struct {
	Market      string      `json:"market"`
	Quantity    float64     `json:"quantity"`
	Rate        float64     `json:"rate"`
	Simulation  bool        `json:"simulation"`
	ClientID    interface{} `json:"clientId"`
	TimingForce interface{} `json:"timingForce"`
}
