package Portfolio

type PortfolioOpenOrder struct {
	Simulation  bool        `json:"simulation"`
	ClientID    string `json:"clientId"`
	TimingForce string `json:"timingForce"`
}
