package Portfolio


type PortfolioCancelCloseOrderRequest struct {
	ID          int         `json:"id"`
	Simulation  bool        `json:"simulation"`
	ClientID    string `json:"clientId"`
	TimingForce string `json:"timingForce"`
}
