module bit4you

go 1.16

require (
	github.com/go-stack/stack v1.8.1 // indirect
	github.com/inconshreveable/log15 v0.0.0-20201112154412-8562bdadbbac // indirect
	github.com/joho/godotenv v1.3.0
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
)
