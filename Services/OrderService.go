package Services

import (
	"bit4you/Models/Order"
)

func OrderList(orderListRequest Order.OrderList) (error, Order.OrderListResponse) {
	result := Order.OrderListResponse{}
	err := ProcessHttpRequest("order/list", "POST", orderListRequest, &result)
	return err, result
}

func OrderInfo(orderInfoRequest Order.OrderInfoRequest) (error, Order.OrderInfoResponse) {
	result := Order.OrderInfoResponse{}
	err := ProcessHttpRequest("order/info", "POST", orderInfoRequest, &result)

	return err, result
}

func OrderPending(orderPendingRequest Order.OrderPendingRequest) (error, Order.OrderPendingResponse) {
	result := Order.OrderPendingResponse{}
	err := ProcessHttpRequest("order/pending", "POST", orderPendingRequest, &result)
	return err, result
}

func OrderCreate(orderCreateRequest Order.OrderCreateRequest) (error, Order.OrderCreateResponse) {
	result := Order.OrderCreateResponse{}
	err := ProcessHttpRequest("order/create", "POST", orderCreateRequest, &result)
	return err, result
}

func OrderCancel(orderCancelRequest Order.OrderCancelRequest) (error, Order.OrderCancelResponse) {
	result := Order.OrderCancelResponse{}
	err := ProcessHttpRequest("order/cancel", "POST", orderCancelRequest, &result)
	return err, result
}
