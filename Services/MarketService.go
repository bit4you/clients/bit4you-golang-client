package Services

import (
	"bit4you/Models/Market"
)

func MarketList() (error, Market.MarketListResponse) {
	result := Market.MarketListResponse{}
	err := ProcessHttpRequest("market/list", "GET", nil, &result)
	return err, result
}

func MarketSummaries() (error, Market.MarketSummaryResponse) {
	result := Market.MarketSummaryResponse{}
	err := ProcessHttpRequest("market/summaries", "GET", nil, &result)
	return err, result
}

func MarketTicks(marketTicksRequest Market.MarketTicksRequest) (error, Market.MarketTicksResponse) {
	result := Market.MarketTicksResponse{}
	err := ProcessHttpRequest("market/ticks", "POST", marketTicksRequest, &result)
	return err, result
}

func MarketOrderBooks(marketOrderBookRequest Market.MarketOrderBookRequest) (error, Market.MarketOrderBookResponse) {
	result := Market.MarketOrderBookResponse{}
	err := ProcessHttpRequest("market/orderbook", "POST", marketOrderBookRequest, &result)
	return err, result
}

func MarketHistory(marketHistoryRequest Market.MarketHistoryRequest) (error, Market.MarketHistoryResponse) {
	result := Market.MarketHistoryResponse{}
	err := ProcessHttpRequest("market/history", "POST", marketHistoryRequest, &result)
	return err, result
}
