package Services

import (
	"bit4you/Models/Portfolio"
)

func PortfolioSummary(portfolioSummaryRequest Portfolio.PortfolioSummaryRequest) (error, Portfolio.PortfolioSummaryResponse) {
	result := Portfolio.PortfolioSummaryResponse{}
	err := ProcessHttpRequest("portfolio/list", "POST", portfolioSummaryRequest, &result)
	return err, result
}

func PortfolioOpenOrder(portfolioOpenOrder Portfolio.PortfolioOpenOrder) (error, Portfolio.PortfolioOpenOrderResponse) {
	result := Portfolio.PortfolioOpenOrderResponse{}
	err := ProcessHttpRequest("portfolio/open-orders", "POST", portfolioOpenOrder, &result)
	return err, result
}

func PortfolioHistory(portfolioHistoryRequest Portfolio.PortfolioHistoryRequest) (error, Portfolio.PortfolioHistoryResponse) {
	result := Portfolio.PortfolioHistoryResponse{}
	err := ProcessHttpRequest("portfolio/history", "POST", portfolioHistoryRequest, &result)
	return err, result
}

func PortfolioCreateOrder(portfolioCreateOrder Portfolio.PortfolioCreateOrder) (error, Portfolio.PortfolioCreateOrderResponse) {
	result := Portfolio.PortfolioCreateOrderResponse{}
	err := ProcessHttpRequest("portfolio/create-order", "POST", portfolioCreateOrder, &result)
	return err, result
}

func PortfolioCloseOrder(portfolioCancelOrderRequest Portfolio.PortfolioCancelCloseOrderRequest) (error, Portfolio.PortfolioCancelCloseResponse) {
	result := Portfolio.PortfolioCancelCloseResponse{}
	err := ProcessHttpRequest("portfolio/close", "POST", portfolioCancelOrderRequest, &result)
	return err, result
}

func PortfolioCancelOrder(portfolioCancelOrderRequest Portfolio.PortfolioCancelCloseOrderRequest) (error, Portfolio.PortfolioCancelCloseResponse) {
	result := Portfolio.PortfolioCancelCloseResponse{}
	err := ProcessHttpRequest("portfolio/cancel-order", "POST", portfolioCancelOrderRequest, &result)
	return err, result
}
