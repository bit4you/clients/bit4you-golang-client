package Services

import (
	"bit4you/Models/Wallet"
)

func WalletBalance(walletBalanceRequest Wallet.WalletBalanceRequest) (error, Wallet.WalletBalanceResponse) {
	result := Wallet.WalletBalanceResponse{}
	err := ProcessHttpRequest("wallet/balances", "POST", walletBalanceRequest, &result)
	return err, result
}

func WalletTransaction(walletTransactionRequest Wallet.WalletTransactionRequest) (error, Wallet.WalletTransactionResponse) {
	result := Wallet.WalletTransactionResponse{}
	err := ProcessHttpRequest("wallet/transactions", "POST", walletTransactionRequest, &result)
	return err, result
}

func WalletSend(walletSendRequest Wallet.WalletSendRequest) (error, Wallet.WalletTransactionResponse) {
	result := Wallet.WalletTransactionResponse{}
	err := ProcessHttpRequest("wallet/send", "POST", walletSendRequest, &result)
	return err, result
}
