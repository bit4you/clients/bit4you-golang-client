package Services

import (
	oauth "bit4you/Models/oAuth"
	"log"
	"os"

	"github.com/joho/godotenv"
)

var initial bool = true

func goDotEnvVariable(key string, def string) string {
	if initial {
		err := godotenv.Load(".env")

		if err != nil {
			log.Fatalf("Error loading .env file")
		}
		initial = false
	}

	val := os.Getenv(key)
	if val == "" {
		return def
	}
	return os.Getenv(key)
}

func GetAccessToken() (error, string) {

	data := oauth.OAuthRequest{
		GrantType: goDotEnvVariable("grant_type", ""),
		Scope:     goDotEnvVariable("scope", ""),
		Username:  goDotEnvVariable("clientid", ""),
		Password:  goDotEnvVariable("clientsecret", ""),
	}

	result := oauth.OAuthResult{}
	err := ProcessHttpRequestPublic("token", "POST", data, &result)
	if err != nil {
		return err, ""
	}

	//
	return nil, result.AccessToken
}

func UserInfo() (error, oauth.ApiUserInfo) {
	result := oauth.ApiUserInfo{}
	err := ProcessHttpRequest("userinfo", "GET", nil, &result)
	return err, result
}
