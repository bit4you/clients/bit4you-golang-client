package Services

import (
	"bytes"
	json "encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

func doHttpUnmarshal(req *http.Request, res interface{}) error {
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	t := struct {
		Error string
	}{}

	if err := json.Unmarshal(body, &t); err == nil && t.Error != "" {
		return errors.New(t.Error)
	} else if err != nil && err.Error() == "unexpected end of JSON input" {
		if len(body) == 0 {
			if resp.StatusCode == 200 {
				return errors.New("empty response")
			}

			return errors.New(resp.Status)
		}

		return errors.New(string(body))
	} else {
		return json.Unmarshal(body, res)
	}
}

func ProcessHttpRequestPublic(api string, method string, data interface{}, result interface{}) error {
	return ProcessHttpRequest(api, method, data, &result, true)
}
func ProcessHttpRequest(api string, method string, data interface{}, result interface{}, notsecure ...bool) error {
	var req *http.Request
	var err error
	var accesstoken string
	if len(notsecure) == 0 {
		err, accesstoken = GetAccessToken()
		if err != nil {
			return err
		}
	}

	url := goDotEnvVariable("apiurl", "https://www.bit4you.io/api/") + api
	if data == nil {
		req, err = http.NewRequest(method, url, nil)
	} else {
		if jsonPayload, err := json.Marshal(data); err == nil {
			req, err = http.NewRequest(method, url, bytes.NewReader(jsonPayload))
		} else {
			return err
		}

	}

	if err != nil {
		return err
	}

	if accesstoken != "" {
		req.Header.Add("Authorization", "bearer "+accesstoken)
	}
	req.Header.Add("Content-Type", "application/json")

	err = doHttpUnmarshal(req, result)
	if err != nil {
		return err
	}

	return nil
}
